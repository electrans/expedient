# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import expedient
from . import party
from . import res
from . import ir


def register():
    Pool.register(
        expedient.Expedient,
        expedient.PhysicalDocuments,
        expedient.ExpedientCompany,
        ir.Attachment,
        party.Party,
        res.Role,
        res.User,
        module='electrans_expedient', type_='model')
