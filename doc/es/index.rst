Expedientes
===========

Modelo
------

Mantenimiento simple de expedientes. Se ha partido del Excel _001llistat_expedient.xlsx_ para establecer los campos necesarios, y se han añadido nuevos:

- *Número*: En el Excel estaban formateados como _001, 002..._; se mostrarán como _1, 2..._
- *Asunto*
- *Descripción*
- *Privado*: Campo nuevo que permitirá ocultar el expediente a quien no tenga permisos de administración de expedientes (ver apartado *permisos*).
- *Documento(s) físicos*: Marca para indicar si el expediente tiene documentos físicos.
- *Documento(s) digitales*: Marca para indicar si el expediente tiene documentos digitales.

Permisos
--------
Se crean 2 grupos nuevos:
*Administrador de expedientes*: Permite ver, crear, modificar y eliminar cualquier expediente.
*Expedientes públicos (solo lectura)*: Permite ver los expedientes no *privados*

Importación
-----------
Mediante *script* se importan los datos del fichero csv (originalmente *001llistat_expedients.xlsx*), 22 de noviembre de 2018. Los registros posteriores a esa fecha deberán incorporarse a mano.