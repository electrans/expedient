from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval


class Attachment(metaclass=PoolMeta):
    __name__ = 'ir.attachment'

    parent = fields.Many2One(
        'ir.attachment', "Folder",
        domain=[
            ('type', '=', 'folder'),
            ('resource', '=', Eval('resource'))],
        depends=['resource'])
    childs = fields.One2Many(
        'ir.attachment', 'parent', "Childs")
    icon = fields.Function(
        fields.Char(" "),
        'get_icon')

    @classmethod
    def __setup__(cls):
        super(Attachment, cls).__setup__()
        folder = ('folder', 'Folder')
        if folder not in cls.type.selection:
            cls.type.selection.append(folder)

    def get_icon(self, name=None):
        if self.type == 'folder':
            return '   '
        elif self.type == 'data':
            return '  '
        else:
            return '    '
