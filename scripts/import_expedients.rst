STEPS TO RUN DE import_expedients.py SCRIPT FROM A TERMINAL
===========================================================

1) Activate de correct environment. In the Electrans server simply change to *electrans* folder; in a working machine use *workon ...*.

	**administrator@erp:~$** cd electrans

2) Set *exports*:

	**(electrans)administrator@erp:~/electrans$** export TRYTOND_CONFIG=/etc/trytond/electrans.conf
	
	**(electrans)administrator@erp:~/electrans$** export PYTHONPATH=trytond:proteus

3) Run the script:

	**(electrans)administrator@erp:~/electrans$** python ./modules/expedient/scripts/import_expedients.py 


**Note:** Once you have imported the csv data, you can set the original create date by executing an *update* query for each row. 
This update queries can be conformed by creating in Excel a calculated column like this:

=CONCATENAR("update expedient set create_date='";A�O([@FECHA]);"-";MES([@FECHA]);"-";DIA([@FECHA]);"' where id=";ENTERO([@[N� EXPED.]]);";")