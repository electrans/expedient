print('Import from proteus...')
import csv
from proteus import config, Model
import os

print('Conectando a BD tryton...')
config.set_trytond('postgresql:///electrans')

print('Abriendo modelo Expedient...')
Expedient = Model.get('expedient')

expedients = []
csv2import = os.getcwd() + '/modules/expedient/scripts/expedients.csv'
print('Abriendo' + csv2import + '...')
with open(csv2import, 'r') as cf:
	print('Importando expedientes.')
	for row in csv.DictReader(cf):
		expedient = Expedient()
		print('Importando expediente ' + row['Subject'])
		expedient.subject = row['Subject']
		expedient.description = row['Description']
		expedients.append(expedient)

print('Salvando...')
Expedient.save(expedients)

print('Fin')
