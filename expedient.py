from trytond.pool import Pool
from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval, If, Not, Bool
from trytond.transaction import Transaction

__all__ = ['Expedient', 'PhysicalDocuments']
_STATES = {'readonly': Eval('state') == 'archived'}


class Expedient(ModelSQL, ModelView):
    """Expedient"""
    __name__ = 'expedient.expedient'

    number = fields.Function(fields.Integer('Number'), 'get_id', searcher='search_id')
    subject = fields.Char('Subject', select=True, states=_STATES)
    description = fields.Text('Description', states=_STATES)
    private = fields.Boolean('Private', help='If marked, only users allowed to see private expedients could see it.',
                             states=_STATES)
    private_icon = fields.Function(fields.Char(' '), 'get_private_icon')
    state = fields.Selection([
            ('draft', 'Draft'),
            ('follow', 'Follow Up'),
            ('archived', 'Archived'),
            ], 'State', select=True, readonly=True)
    company = fields.Many2One('expedient.company', 'Company Expedient',
                              states=_STATES)
    party = fields.Many2One('party.party', 'Party Expedient', domain=[('expedients_party', '=', True)], states=_STATES)
    fiscal_year = fields.Many2One('account.fiscalyear', 'Fiscal Year', states=_STATES)
    count_attachments = fields.Function(fields.Char('Digital Documents'), 'get_count_attachments')
    digital_documents = fields.One2Many('ir.attachment', 'resource', 'Digital Documents',
                                        filter=[
                                            ('parent', '=', None),
                                        ])
    documents_location = fields.Many2One('stock.location', 'Physical Documents Location', states=_STATES)
    physical_documents = fields.One2Many('expedient.document', 'expedient', 'Physical Documents')
    allowed_role = fields.Many2One(
        'res.role', "Role",
        states={
            'invisible': Not(Eval('private')),
            'required': Bool(Eval('private'))},
        domain=[('id', 'in', Eval('context', {}).get('user_roles', []))],
        depends=['private'])

    @classmethod
    def __register__(cls, module_name):
        super().__register__(module_name)
        table_h = cls.__table_handler__(module_name)
        table_h.drop_column('type')

    @classmethod
    def __setup__(cls):
        super(Expedient, cls).__setup__()
        cls._buttons.update({
                'draft': {
                    'invisible': Eval('state') == 'draft',
                    'icon': "tryton-go-previous"
                },
                'follow': {
                    'invisible': Eval('state') == 'follow',
                    'icon': If(Eval('state') == 'archived',
                               'tryton-go-previous', 'tryton-go-next')
                    },
                'archive': {
                    'invisible': Eval('state') == 'archived',
                    },
                })

    @staticmethod
    def default_state():
        return 'draft'

    def get_rec_name(self, name):
        subject = ' (' + self.subject + ')' if self.subject else ''
        return str(self.number) + subject

    @classmethod
    def search_rec_name(cls, name, clause):
        return [
            'OR',
            ('subject',) + tuple(clause[1:]),
            ('description',) + tuple(clause[1:]),
            ('digital_documents.name',) + tuple(clause[1:]),
            ('physical_documents.name',) + tuple(clause[1:]),
            ('party.name',) + tuple(clause[1:]),
            ('company.name',) + tuple(clause[1:])]

    def get_id(self, name=None):
        return self.id

    @classmethod
    def search_id(cls, name, clause):
        return [('id',) + tuple(clause[1:])]

    @classmethod
    @ModelView.button
    def draft(cls, servers):
        cls.write(servers, {
                'state': 'draft',
                })

    @classmethod
    @ModelView.button
    def follow(cls, servers):
        cls.write(servers, {
                'state': 'follow',
                })

    @classmethod
    @ModelView.button
    def archive(cls, servers):
        cls.write(servers, {
                'state': 'archived',
                })

    def get_private_icon(self, name=None):
        if self.private:
            return " "

    def get_count_attachments(self, name=None):
        Attachment = Pool().get('ir.attachment')
        atts = Attachment.search([('resource', '=', 'expedient.expedient,%s' % self.id), ('type', '=', 'data')])
        return '( ' + str(len(atts)) + ' )'

    @fields.depends('private', 'user_roles')
    def on_change_with_allowed_role(self):
        if self.private:
            roles = [role.id for role in Pool().get('res.user')(Transaction().user).user_roles]
            return roles[0] if roles else None
            return self.user_roles[0].id if self.user_roles else None
        return None


class PhysicalDocuments(ModelSQL, ModelView):
    """Expedient Document"""
    __name__ = 'expedient.document'

    name = fields.Char('Name')
    expedient = fields.Many2One('expedient.expedient', 'Expedient')


class ExpedientCompany(ModelSQL, ModelView):
    """Expedient Company"""
    __name__ = 'expedient.company'

    name = fields.Char('Name')
