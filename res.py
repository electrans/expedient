# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction


class Role(metaclass=PoolMeta):
    __name__ = 'res.role'

    roles_rel = fields.One2Many(
        'res.user.role', 'role', "User Rel")


class User(metaclass=PoolMeta):
    __name__ = 'res.user'

    user_roles = fields.Function(
        fields.One2Many(
            'res.role', None,
            "User Roles",
            depends=['roles']),
        'get_user_roles')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._context_fields.append('user_roles')

    def get_user_roles(self, name=None):
        pool = Pool()
        User = pool.get('res.user')
        userRole = pool.get('res.user.role')
        Date = pool.get('ir.date')
        today = Date.today()
        res = []
        roles = User(Transaction().user).roles
        for role in roles:
            if role.valid(today):
                res.append(userRole(role).role.id)
        return res
